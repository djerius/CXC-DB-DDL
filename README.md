# NAME

CXC::DB::DDL - DDL for table creation, based on SQL::Translator::Schema

# VERSION

version 0.19

# DESCRIPTION

**CXC::DB::DDL** provides a procedural interface to generating DDL to create and drop database tables.

It uses [SQL::Translator](https://metacpan.org/pod/SQL%3A%3ATranslator) to create the required SQL, and provides a
little bit of extra DSL sauce in [CXC::DB::DDL::Util](https://metacpan.org/pod/CXC%3A%3ADB%3A%3ADDL%3A%3AUtil).

See [CXC::DB:DDL::Manual::Intro](https://metacpan.org/pod/CXC%3A%3ADB%3ADDL%3A%3AManual%3A%3AIntro).

# METHODS

## table

    # return CXC::DB::DDL::Table object for named table
    $table = $ddl->table( $table_name );

    # If only one table, return CXC::DB::DDL::Table object for it
    $table = $ddl->table;

Returns `undef` if no matching table or no tables.

## tables

    \@tables = $ddl->tables

Return the tables.

## add\_table

    $ddl->add_table( InstanceOf ['CXC::DB::DDL::Table'] );

## sql

    $ddl->sql( $dbh, %options )

Create the SQL associated with the `$ddl` object using the [DBI](https://metacpan.org/pod/DBI)
database handle, `$dbh`. Various options may be specified:

- create

    One of the constants (see <CXC::DB::DDL::Constants>) `CREATE_ONCE`,
    `CREATE_ALWAYS`, `CREATE_IF_NOT_EXISTS`.

- sqlt\_comments _Bool_

    Add comments in SQL \[Default: true\].

- sqlt\_quote\_identifiers _Bool_

    Quote identifiers (Default: producer specific)

- sqlt\_debug _Bool_

    [SQL::Translator](https://metacpan.org/pod/SQL%3A%3ATranslator) constructor `debug` option.

- sqlt\_trace

    [SQL::Translator](https://metacpan.org/pod/SQL%3A%3ATranslator) constructor `trace` option.

## create

    $ddl->create( $dbh, ?$create = CREATE_ONCE | CREATE_ALWAYS | CREATE_IF_NOT_EXISTS );

Create the tables associated with the `$ddl` object using the [DBI](https://metacpan.org/pod/DBI)
database handle, `$dbh`.  `$create` defaults to `CREATE_ONCE`;

**CREATE\_ALWAYS** drops the tables and creates them.

**CREATE\_IF\_NOT\_EXISTS** checks if there are any missing tables; if not it returns.

**CREATE\_ONCE** will attempt to create the tables, and will throw an exception
if one exists.

Returns true if tables were created.

## clear

    @not_cleared = $ddl->clear( $dbh );

Clear the contents of the tables in the DDL schema.  DDL::Table
objects for tables which do not exist are returned.

## drop

    $ddl->drop( $dbh );

Drop all of the tables in the DDL schema in reverse order of addition.

## clone\_simple

    $cloned_ddl = $ddl->clone_simple

Return a new object based on the current one, running
[CXC::DB::DDL::Table::clone\_simple](https://metacpan.org/pod/CXC%3A%3ADB%3A%3ADDL%3A%3ATable%3A%3Aclone_simple) on the tables.

# SUPPORT

## Bugs

Please report any bugs or feature requests to bug-cxc-db-ddl@rt.cpan.org  or through the web interface at: [https://rt.cpan.org/Public/Dist/Display.html?Name=CXC-DB-DDL](https://rt.cpan.org/Public/Dist/Display.html?Name=CXC-DB-DDL)

## Source

Source is available at

    https://gitlab.com/djerius/cxc-db-ddl

and may be cloned from

    https://gitlab.com/djerius/cxc-db-ddl.git

# SEE ALSO

Please see those modules/websites for more information related to this module.

- [CXC::DB::DDL::Manual::Intro](https://metacpan.org/pod/CXC%3A%3ADB%3A%3ADDL%3A%3AManual%3A%3AIntro)

# AUTHOR

Diab Jerius <djerius@cpan.org>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2022 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007
