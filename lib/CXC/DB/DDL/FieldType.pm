package CXC::DB::DDL::FieldType;

# ABSTRACT: Class for non-DBI types

use v5.26;

use strict;
use warnings;
use experimental 'signatures', 'postderef';

our $VERSION = '0.19';

use constant { NAME => 0, TYPE => 1 };

=constructor new

=cut


sub new ( $class, $name, $type ) {
    return bless [ $name, $type ], $class;
}

=method name

  $name = $type->name;

Return the name of the type.

=cut


sub name ( $self ) { $self->[NAME]; }

=method type

  $type = $type->type;

Return the type code of the type.

=cut

sub type ( $self ) { $self->[TYPE]; }

1;
