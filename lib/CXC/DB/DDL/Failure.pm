package CXC::DB::DDL::Failure;

# ABSTRACT: Failure classes for App::Deosc

use v5.26;
use strict;
use warnings;

our $VERSION = '0.19';

use custom::failures qw(
  parameter_constraint
  duplicate
  create
  ddl
);

1;

# COPYRIGHT

__END__

=head1 DESCRIPTION

This module creates custom failure classes

=over

=item CXC::DB::DDL::Failure::parameter_constraint

=item CXC::DB::DDL::Failure::duplicate

=item CXC::DB::DDL::Failure::create

=item CXC::DB::DDL::Failure::ddl

=back

=cut
