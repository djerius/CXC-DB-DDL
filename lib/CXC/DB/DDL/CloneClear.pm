package CXC::DB::DDL::CloneClear;

# ABSTRACT: Provide attribute tags and a method for Moo objects to indicate they should be cloned

use v5.26;

use Moo::Role;

use MooX::TaggedAttributes -propagate, -tags => 'cloneclear';
use experimental 'signatures', 'postderef';

use namespace::clean -except => [ '_tag_list', '_tags' ];

our $VERSION = '0.19';

=method clone

  $clone = $obj->clone;

Clone the object.  Uses L<Clone>.

=cut

sub clone ( $self ) {
    require Clone;
    Clone::clone( $self );
}

=method clone_simple

   $clone = $obj->clone_simple;

Return a clone of the object without any external constraints or auto
increment properties.  Primary key constraints remain.

The clear attribute method is run on object attributes which have a
C<cloneclear> tag.

=cut


sub clone_simple ( $self ) {
    my $clone = $self->clone;
    $clone->$_ for map { "clear_$_" } keys $self->_tags->{cloneclear}->%*;
    return $clone;
}

1;

# COPYRIGHT

__END__

=head1 SYNOPSIS

  package Class {
    use Moo;
    with "CXC::DB::DDL::CloneClear';

    has attr1 => ( is => 'rw', default => 'A1', cloneclear => 1 );
    has attr2 => ( is => 'rw', default => 'A2' );
  }

  my $obj =  Class->new( attr1 => 'B1', attr2 => 'B2' );
  my $clone = $obj->clone_simple;

  say $clone->attr1; # A1
  say $clone->attr2; # B2

=cut
