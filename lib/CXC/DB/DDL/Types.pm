package CXC::DB::DDL::Types;

# ABSTRACT: Types, oh my!

use v5.26;
use strict;
use warnings;

our $VERSION = '0.19';

use CXC::DB::DDL::Constants -all;

use Type::Library -base, -declare => ( qw(
      Index
      Indexes
      Constraint
) );

use Type::Utils -all;
use Types::Standard qw( ArrayRef Bool CodeRef Dict Enum HashRef Optional Str );


=type Indexes

An array of values which pass the L</Index> type constraint.

Coercible from an B<< ArrayRef [Str] >> as

    [ { fields => $aref } ]

=for stopwords
coercible

=cut

declare Indexes, as ArrayRef [Index];

coerce Indexes, from ArrayRef [Str], q/ [ { fields => $_ } ] /;

=type Index

A hashref with the following fields (all of which are optional, but
that would result in a bogus constraint, so don't do that):

=over

=item name    => Optional [Str]

Z<>

=item fields  => Optional [ Str | ArrayRef [Str] ]

Z<>

=item type    => Optional [ Enum [SCHEMA_CONSTANTS] ]

Z<>

=item options => Optional [HashRef]

=back

=cut

declare Index,
  as Dict [
    name    => Optional [Str],
    fields  => Optional [ Str | ArrayRef [Str] ],
    type    => Optional [ Enum [SCHEMA_CONSTANTS] ],
    options => Optional [HashRef],
  ];

coerce Index, from ArrayRef [Str], q[ { fields => $_ } ];


=type Constraint

A hashref with the following fields (all of which are optional, but
that would result in a bogus constraint, so don't do that):

=over

=item type              => Optional [ Enum [SCHEMA_CONSTANTS] ]

Z<>

=item name              => Optional [Str]

Z<>

=for stopwords
deferrable

=item deferrable        => Optional [Bool]

Z<>

=item expression        => Optional [Str|CodeRef]

Z<>

=item fields            => Optional [ Str | ArrayRef [Str] ]

Z<>

=item referenced_fields => Optional [ Str | ArrayRef [Str] ]

Z<>

=item reference_table   => Optional [Str]

Z<>

=item match_type        => Optional [ Enum [SCHEMA_CONSTRAINT_MATCH_TYPES] ]

Z<>

=item on_delete         => Optional [ Enum [SCHEMA_CONSTRAINT_ON_DELETE] ]

Z<>

=item on_update         => Optional [ Enum [SCHEMA_CONSTRAINT_ON_UPDATE] ]

Z<>

=item options           => Optional [HashRef]

Z<>

=back

=cut

declare Constraint,
  as Dict [
    type              => Enum [SCHEMA_CONSTANTS],
    name              => Optional [Str],
    deferrable        => Optional [Bool],
    expression        => Optional [ Str | CodeRef ],
    fields            => Optional [ Str | ArrayRef [Str] ],
    referenced_fields => Optional [ Str | ArrayRef [Str] ],
    reference_table   => Optional [Str],
    match_type        => Optional [ Enum [SCHEMA_CONSTRAINT_MATCH_TYPES] ],
    on_delete         => Optional [ Enum [SCHEMA_CONSTRAINT_ON_DELETE] ],
    on_update         => Optional [ Enum [SCHEMA_CONSTRAINT_ON_UPDATE] ],
    options           => Optional [HashRef],
  ],
  coercion => 1;


1;

# COPYRIGHT

__END__


=head1 SYNOPSIS

  use CXC::DB::DDL::Types -all;

  say "yup, it's an index" if is_Index($index);

=head1 DESCRIPTION

Types used by L<CXC::DB::DDL> and associated modules.  Enumerated
values are typically generated by the enumeration functions in
L<CXC::DB::DDL::Constants>; see that for more information.
